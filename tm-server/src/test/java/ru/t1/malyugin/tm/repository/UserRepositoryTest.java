package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class UserRepositoryTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    private static final List<User> USER_LIST = new ArrayList<>();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("LOGIN " + i);
            @Nullable final String hash = HashUtil.salt(PROPERTY_SERVICE, "password" + i);
            Assert.assertNotNull(hash);
            user.setPasswordHash(hash);
            user.setEmail("mail@" + i);
            USER_REPOSITORY.add(user);
            USER_LIST.add(user);
        }
    }

    @After
    public void clearData() {
        USER_REPOSITORY.clear();
        USER_LIST.clear();
    }

    @Test
    public void testAddUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final User user = new User();
        USER_REPOSITORY.add(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            userList.add(new User());
        }
        USER_REPOSITORY.addAll(userList);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void create() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String login1 = "NEW_LOGIN";
        @Nullable final String passwordHash = HashUtil.salt(PROPERTY_SERVICE, "NEW_PASS");
        Assert.assertNotNull(passwordHash);
        @NotNull final User user1 = USER_REPOSITORY.create(login1, passwordHash);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
        Assert.assertEquals(login1, user1.getLogin());
        Assert.assertEquals(passwordHash, user1.getPasswordHash());
        Assert.assertNull(user1.getEmail());

        expectedNumberOfEntries++;
        @NotNull final String login2 = "OLD_LOGIN";
        @NotNull final String email = "TEST_MAIL";
        @NotNull final Role role = Role.ADMIN;
        @NotNull final User user2 = USER_REPOSITORY.create(login2, passwordHash, email, role);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
        Assert.assertEquals(login2, user2.getLogin());
        Assert.assertEquals(passwordHash, user2.getPasswordHash());
        Assert.assertEquals(email, user2.getEmail());
        Assert.assertEquals(role, user2.getRole());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        USER_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> userList = USER_REPOSITORY.findAll();
        Assert.assertEquals(USER_LIST, userList);
    }

    @Test
    public void testFindOneById() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user, USER_REPOSITORY.findOneById(user.getId()));
        }
        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(USER_REPOSITORY.findOneById(id));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user, USER_REPOSITORY.findOneByLogin(user.getLogin()));
        }
        @NotNull final String login = "TEST_LOGIN";
        Assert.assertNull(USER_REPOSITORY.findOneByLogin(login));
    }

    @Test
    public void testFindOneByEmail() {
        for (@NotNull final User user : USER_LIST) {
            if (user.getEmail() != null) Assert.assertEquals(user, USER_REPOSITORY.findOneByEmail(user.getEmail()));
        }
        @NotNull final String email = "TEST_EMAIL";
        Assert.assertNull(USER_REPOSITORY.findOneByEmail(email));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < USER_REPOSITORY.getSize(); i++) {
            Assert.assertEquals(USER_LIST.get(i), USER_REPOSITORY.findOneByIndex(i));
        }

        Assert.assertNull(USER_REPOSITORY.findOneByIndex(-1));
        Assert.assertNull(USER_REPOSITORY.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertTrue(USER_REPOSITORY.isLoginExist(user.getLogin()));
        }
        @NotNull final String login = "TEST_LOGIN";
        Assert.assertFalse(USER_REPOSITORY.isLoginExist(login));
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : USER_LIST) {
            if (user.getEmail() != null) Assert.assertTrue(USER_REPOSITORY.isEmailExist(user.getEmail()));
        }
        @NotNull final String email = "TEST_EMAIL";
        Assert.assertFalse(USER_REPOSITORY.isEmailExist(email));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(USER_LIST.size(), USER_REPOSITORY.getSize());
    }

    @Test
    public void testSet() {
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            userList.add(new User());
        }
        USER_REPOSITORY.set(userList);
        Assert.assertEquals(userList, USER_REPOSITORY.findAll());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_REPOSITORY.remove(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
        Assert.assertNull(USER_REPOSITORY.findOneById(user.getId()));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_REPOSITORY.removeById(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
        Assert.assertNull(USER_REPOSITORY.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        USER_REPOSITORY.removeByIndex(index);
        Assert.assertNull(USER_REPOSITORY.removeByIndex(-1));
        Assert.assertNull(USER_REPOSITORY.removeByIndex(NUMBER_OF_ENTRIES + 1));
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

}