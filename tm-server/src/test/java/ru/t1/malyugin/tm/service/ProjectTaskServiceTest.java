package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IProjectTaskService;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.malyugin.tm.exception.field.TaskIdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;
    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @NotNull
    private static final IProjectTaskService PROJECT_TASK_SERVICE = new ProjectTaskService(PROJECT_REPOSITORY, TASK_REPOSITORY);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @Before
    public void initTest() {
        @Nullable String projectIdToBind = null;
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project("P" + i, "D" + i);
            @NotNull final Task task = new Task("T" + i, "D" + i);
            if (i == 1) projectIdToBind = project.getId();
            if (i <= 5) {
                project.setUserId(USER_ID_1);
                task.setUserId(USER_ID_1);
            } else {
                project.setUserId(USER_ID_2);
                task.setUserId(USER_ID_2);
            }
            PROJECT_REPOSITORY.add(project);
            TASK_REPOSITORY.add(task);
            if (i <= 3) {
                task.setProjectId(projectIdToBind);
            }
        }
    }

    @After
    public void clearData() {
        PROJECT_REPOSITORY.clear();
        TASK_REPOSITORY.clear();
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final String projectId = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0).getId();
        @NotNull final List<Task> taskListToRemove = TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId);
        final int expectedNumberOfProjects = NUMBER_OF_ENTRIES - 1;
        final int expectedNumberOfTasks = TASK_REPOSITORY.getSize(USER_ID_1) - taskListToRemove.size();
        PROJECT_TASK_SERVICE.removeProjectById(USER_ID_1, projectId);
        Assert.assertEquals(expectedNumberOfProjects, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(expectedNumberOfTasks, TASK_REPOSITORY.findAll(USER_ID_1).size());
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(null, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(UNKNOWN_ID, UNKNOWN_ID));
    }

    @Test
    public void testRemoveProjectByIndex() {
        @Nullable final Project project = PROJECT_REPOSITORY.findOneByIndex(USER_ID_1, 0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> taskListToRemove = TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId);
        final int expectedNumberOfProjects = NUMBER_OF_ENTRIES - 1;
        final int expectedNumberOfTasks = TASK_REPOSITORY.getSize(USER_ID_1) - taskListToRemove.size();
        PROJECT_TASK_SERVICE.removeProjectByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfProjects, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(expectedNumberOfTasks, TASK_REPOSITORY.findAll(USER_ID_1).size());
    }

    @Test
    public void testRemoveProjectByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectByIndex(null, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_TASK_SERVICE.removeProjectByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_TASK_SERVICE.removeProjectByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_TASK_SERVICE.removeProjectByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES));
    }

    @Test
    public void testBindTaskToProject() {
        final String projectId = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0).getId();
        final String taskId = TASK_REPOSITORY.findAll(USER_ID_1).get(4).getId();
        final int actualNumberOfEntries = TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId).size();
        final int expectedNumberOfEntries = actualNumberOfEntries + 1;
        Assert.assertNotNull(PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID_1, projectId, taskId));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId).size());
        @Nullable final Task task = TASK_REPOSITORY.findOneById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(UNKNOWN_ID, UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        @NotNull final String projectId = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID_1, projectId, UNKNOWN_ID));
    }

    @Test
    public void testUnbindTaskToProject() {
        final String projectId = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0).getId();
        final String taskId = TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId).get(0).getId();
        final int actualNumberOfEntries = TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId).size();
        final int expectedNumberOfEntries = actualNumberOfEntries - 1;
        Assert.assertNotNull(PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID_1, projectId, taskId));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.findAllByProjectId(USER_ID_1, projectId).size());
        Assert.assertNull(TASK_REPOSITORY.findOneById(taskId).getProjectId());
    }

    @Test
    public void testUnbindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(UNKNOWN_ID, UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        @NotNull final String projectId = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID_1, projectId, UNKNOWN_ID));
    }

}