package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.api.service.ISessionService;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final List<Session> SESSION_LIST = new ArrayList<>();

    @NotNull
    private static final ISessionRepository SESSION_REPOSITORY = new SessionRepository();

    @NotNull
    private static final ISessionService SESSION_SERVICE = new SessionService(SESSION_REPOSITORY);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            SESSION_REPOSITORY.add(session);
            SESSION_LIST.add(session);
        }
    }

    @After
    public void clearData() {
        SESSION_REPOSITORY.clear();
        SESSION_LIST.clear();
    }

    @Test
    public void testAdd() {
        Assert.assertNull(SESSION_SERVICE.add(null));
        Assert.assertEquals(NUMBER_OF_ENTRIES, SESSION_SERVICE.getSize());
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        Assert.assertNotNull(SESSION_SERVICE.add(new Session()));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(SESSION_LIST.size(), SESSION_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final int actualSessionList = SESSION_SERVICE.getSize(USER_ID_1);
        Assert.assertEquals(actualSessionList, SESSION_SERVICE.getSize(USER_ID_1));
        Assert.assertEquals(0, SESSION_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.getSize(null));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        SESSION_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        SESSION_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_ENTRIES, SESSION_SERVICE.getSize());

        final int expectedNumberOfEntries = 0;
        SESSION_SERVICE.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize(USER_ID_1));
        Assert.assertNotEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.clear(null));
    }

    @Test
    public void testAddAll() {
        Assert.assertEquals(Collections.emptyList(), SESSION_SERVICE.addAll(null));
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Session> sessionListToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) sessionListToAdd.add(new Session());
        Assert.assertNotNull(SESSION_SERVICE.addAll(sessionListToAdd));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testSet() {
        Assert.assertEquals(Collections.emptyList(), SESSION_SERVICE.set(null));
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final List<Session> sessionListToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES + 1; i++) sessionListToAdd.add(new Session());
        Assert.assertNotNull(SESSION_SERVICE.set(sessionListToAdd));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        Assert.assertNotNull(SESSION_SERVICE.remove(session));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> SESSION_SERVICE.remove(null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        Assert.assertNotNull(SESSION_SERVICE.removeById(session.getId()));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(null));
        Assert.assertThrows(EntityNotFoundException.class, () -> SESSION_SERVICE.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(SESSION_SERVICE.removeById(USER_ID_1, session.getId()));
        Assert.assertNull(SESSION_SERVICE.findOneById(session.getId()));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.removeById(null, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(UNKNOWN_ID, null));
        Assert.assertThrows(EntityNotFoundException.class, () -> SESSION_SERVICE.removeById(UNKNOWN_ID, UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @Nullable final Session session = SESSION_SERVICE.findOneByIndex(0);
        Assert.assertNotNull(session);
        Assert.assertNotNull(SESSION_SERVICE.removeByIndex(0));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
        Assert.assertNull(SESSION_REPOSITORY.findOneById(session.getId()));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.removeByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.removeByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(SESSION_SERVICE.removeByIndex(USER_ID_1, 0));
        Assert.assertNull(SESSION_SERVICE.findOneById(session.getId()));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.removeByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.removeByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.removeByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.removeByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 2;
        @NotNull final List<Session> sessionListToRemove = new ArrayList<>();
        sessionListToRemove.add(SESSION_SERVICE.findAll().get(0));
        sessionListToRemove.add(SESSION_SERVICE.findAll().get(1));
        SESSION_SERVICE.removeAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, SESSION_SERVICE.getSize());
        SESSION_SERVICE.removeAll(sessionListToRemove);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
        for (Session session : sessionListToRemove) Assert.assertNull(SESSION_SERVICE.findOneById(session.getId()));
    }

    @Test
    public void testFindOneById() {
        Assert.assertNull(SESSION_SERVICE.findOneById(UNKNOWN_ID));
        Assert.assertNull(SESSION_SERVICE.findOneById(null));
        for (@NotNull final Session session : SESSION_LIST)
            Assert.assertEquals(session, SESSION_SERVICE.findOneById(session.getId()));
    }

    @Test
    public void testFindOneByIdForUser() {
        Assert.assertNull(SESSION_SERVICE.findOneById(null));
        Assert.assertNull(SESSION_SERVICE.findOneById(UNKNOWN_ID));
        for (@NotNull final Session session : SESSION_SERVICE.findAll(USER_ID_1))
            Assert.assertEquals(session, SESSION_SERVICE.findOneById(USER_ID_1, session.getId()));
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.findOneById(null, UNKNOWN_ID));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < SESSION_SERVICE.getSize(); i++)
            Assert.assertEquals(SESSION_LIST.get(i), SESSION_SERVICE.findOneByIndex(i));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.findOneByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        for (int i = 0; i < SESSION_SERVICE.getSize(USER_ID_1); i++)
            Assert.assertEquals(SESSION_SERVICE.findAll(USER_ID_1).get(i), SESSION_SERVICE.findOneByIndex(USER_ID_1, i));
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.findOneByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.findOneByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.findOneByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> SESSION_SERVICE.findOneByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> actualSessionList = SESSION_SERVICE.findAll();
        Assert.assertEquals(SESSION_LIST, actualSessionList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> actualSessionList = SESSION_SERVICE.findAll(USER_ID_1);
        @NotNull final List<Session> expectedSessionList = SESSION_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedSessionList, actualSessionList);
    }

    @Test
    public void testFindAllForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.findAll((String) null));
    }

}