package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class SessionRepositoryTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    private static final List<Session> SESSION_LIST = new ArrayList<>();

    @NotNull
    private static final ISessionRepository SESSION_REPOSITORY = new SessionRepository();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            SESSION_REPOSITORY.add(session);
            SESSION_LIST.add(session);
        }
    }

    @After
    public void clearData() {
        SESSION_LIST.clear();
        SESSION_REPOSITORY.clear();
    }

    @Test
    public void testAddSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session session = new Session();
        SESSION_REPOSITORY.add(session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testAddSessionForUser() {
        final int expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_1) + 1;
        @NotNull final Session session = new Session();
        SESSION_REPOSITORY.add(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.findOneById(USER_ID_2, session.getId()));
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessionList.add(new Session());
        }
        SESSION_REPOSITORY.addAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        SESSION_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testClearForUser() {
        final int expectedNumberOfEntries = 0;

        SESSION_REPOSITORY.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_ENTRIES, SESSION_REPOSITORY.getSize());

        SESSION_REPOSITORY.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNotEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_2));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessionList = SESSION_REPOSITORY.findAll();
        Assert.assertEquals(SESSION_LIST, sessionList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> actualSessionList = SESSION_REPOSITORY.findAll(USER_ID_1);
        @NotNull final List<Session> expectedSessionList = SESSION_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedSessionList, actualSessionList);
    }

    @Test
    public void testFindOneById() {
        for (@NotNull final Session session : SESSION_LIST) {
            Assert.assertEquals(session, SESSION_REPOSITORY.findOneById(session.getId()));
        }
        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(SESSION_REPOSITORY.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final List<Session> userSessionList = SESSION_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (@NotNull final Session session : userSessionList) {
            Assert.assertEquals(session, SESSION_REPOSITORY.findOneById(USER_ID_1, session.getId()));
            Assert.assertNull(SESSION_REPOSITORY.findOneById(USER_ID_2, session.getId()));
        }

        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(SESSION_REPOSITORY.findOneById(USER_ID_1, id));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < SESSION_REPOSITORY.getSize(); i++) {
            Assert.assertEquals(SESSION_LIST.get(i), SESSION_REPOSITORY.findOneByIndex(i));
        }

        Assert.assertNull(SESSION_REPOSITORY.findOneByIndex(-1));
        Assert.assertNull(SESSION_REPOSITORY.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Session> userSessionList = SESSION_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userSessionList.size(); i++) {
            Assert.assertEquals(userSessionList.get(i), SESSION_REPOSITORY.findOneByIndex(USER_ID_1, i));
            Assert.assertNotEquals(userSessionList.get(i), SESSION_REPOSITORY.findOneByIndex(USER_ID_2, i));
        }

        Assert.assertNull(SESSION_REPOSITORY.findOneByIndex(USER_ID_1, -1));
        Assert.assertNull(SESSION_REPOSITORY.findOneByIndex(USER_ID_1, userSessionList.size() + 1));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(SESSION_LIST.size(), SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final int userSessionListSize = (int) SESSION_LIST.stream().filter(p -> USER_ID_1.equals(p.getUserId())).count();
        Assert.assertEquals(userSessionListSize, SESSION_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testSet() {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessionList.add(new Session());
        }
        SESSION_REPOSITORY.set(sessionList);
        Assert.assertEquals(sessionList, SESSION_REPOSITORY.findAll());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_REPOSITORY.remove(session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
        Assert.assertNull(SESSION_REPOSITORY.findOneById(session.getId()));
    }

    @Test
    public void testRemoveForUser() {
        int expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Session session1 = SESSION_REPOSITORY.findAll(USER_ID_1).get(0);

        SESSION_REPOSITORY.remove(USER_ID_1, session1);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.findOneById(USER_ID_1, session1.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, SESSION_REPOSITORY.getSize());

        expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_2);
        @NotNull final Session session2 = SESSION_REPOSITORY.findAll(USER_ID_2).get(0);

        SESSION_REPOSITORY.remove(USER_ID_1, session2);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_REPOSITORY.removeById(session.getId());
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
        Assert.assertNull(SESSION_REPOSITORY.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        int expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_1) - 1;

        @NotNull final Session session1 = SESSION_REPOSITORY.findAll(USER_ID_1).get(0);
        SESSION_REPOSITORY.removeById(USER_ID_1, session1.getId());
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.findOneById(session1.getId()));

        expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_2);
        @NotNull final Session session2 = SESSION_REPOSITORY.findAll(USER_ID_2).get(0);
        SESSION_REPOSITORY.removeById(USER_ID_1, session2.getId());
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_2));

        expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        SESSION_REPOSITORY.removeByIndex(index);
        Assert.assertNull(SESSION_REPOSITORY.removeByIndex(-1));
        Assert.assertNull(SESSION_REPOSITORY.removeByIndex(NUMBER_OF_ENTRIES + 1));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        int expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Session session = SESSION_REPOSITORY.findAll(USER_ID_1).get(0);
        int index = SESSION_LIST.indexOf(session);

        Assert.assertNotNull(SESSION_REPOSITORY.removeByIndex(USER_ID_1, index));
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.findOneById(session.getId()));
        Assert.assertNull(SESSION_REPOSITORY.removeByIndex(USER_ID_1, -1));
        Assert.assertNull(SESSION_REPOSITORY.removeByIndex(USER_ID_1, SESSION_REPOSITORY.getSize(USER_ID_1) + 1));
        expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

}