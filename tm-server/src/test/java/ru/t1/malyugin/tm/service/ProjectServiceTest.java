package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import java.util.*;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(PROJECT_REPOSITORY);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_STRING = "SOME_STR";

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project("P" + i, "D" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        PROJECT_REPOSITORY.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testAdd() {
        Assert.assertNull(PROJECT_SERVICE.add(null));
        Assert.assertEquals(NUMBER_OF_ENTRIES, PROJECT_SERVICE.getSize());
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        Assert.assertNotNull(PROJECT_SERVICE.add(new Project()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final int actualProjectList = PROJECT_SERVICE.getSize(USER_ID_1);
        Assert.assertEquals(actualProjectList, PROJECT_SERVICE.getSize(USER_ID_1));
        Assert.assertEquals(0, PROJECT_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.getSize(null));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        PROJECT_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_ENTRIES, PROJECT_SERVICE.getSize());

        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(USER_ID_1));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.clear(null));
    }

    @Test
    public void testAddAll() {
        Assert.assertEquals(Collections.emptyList(), PROJECT_SERVICE.addAll(null));
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Project> PROJECT_LISTToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) PROJECT_LISTToAdd.add(new Project());
        Assert.assertNotNull(PROJECT_SERVICE.addAll(PROJECT_LISTToAdd));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testSet() {
        Assert.assertEquals(Collections.emptyList(), PROJECT_SERVICE.set(null));
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final List<Project> PROJECT_LISTToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES + 1; i++) PROJECT_LISTToAdd.add(new Project());
        Assert.assertNotNull(PROJECT_SERVICE.set(PROJECT_LISTToAdd));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertNotNull(PROJECT_SERVICE.remove(project));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.remove(null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeById(project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.removeById(null));
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeById(USER_ID_1, project.getId()));
        Assert.assertNull(PROJECT_SERVICE.findOneById(project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(null, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID, null));
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID, UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_SERVICE.findAll().get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeByIndex(0));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project.getId()));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeByIndex(USER_ID_1, 0));
        Assert.assertNull(PROJECT_SERVICE.findOneById(project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 2;
        @NotNull final List<Project> PROJECT_LISTToRemove = new ArrayList<>();
        PROJECT_LISTToRemove.add(PROJECT_SERVICE.findAll().get(0));
        PROJECT_LISTToRemove.add(PROJECT_SERVICE.findAll().get(1));
        PROJECT_SERVICE.removeAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, PROJECT_SERVICE.getSize());
        PROJECT_SERVICE.removeAll(PROJECT_LISTToRemove);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        for (Project project : PROJECT_LISTToRemove) Assert.assertNull(PROJECT_SERVICE.findOneById(project.getId()));
    }

    @Test
    public void testFindOneById() {
        Assert.assertNull(PROJECT_SERVICE.findOneById(UNKNOWN_ID));
        Assert.assertNull(PROJECT_SERVICE.findOneById(null));
        for (@NotNull final Project project : PROJECT_LIST)
            Assert.assertEquals(project, PROJECT_SERVICE.findOneById(project.getId()));
    }

    @Test
    public void testFindOneByIdForUser() {
        Assert.assertNull(PROJECT_SERVICE.findOneById(null));
        Assert.assertNull(PROJECT_SERVICE.findOneById(UNKNOWN_ID));
        for (@NotNull final Project project : PROJECT_SERVICE.findAll(USER_ID_1))
            Assert.assertEquals(project, PROJECT_SERVICE.findOneById(USER_ID_1, project.getId()));
        Assert.assertNull(PROJECT_SERVICE.findOneById(USER_ID_1, null));
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneById(null, UNKNOWN_ID));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < PROJECT_SERVICE.getSize(); i++)
            Assert.assertEquals(PROJECT_LIST.get(i), PROJECT_SERVICE.findOneByIndex(i));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        for (int i = 0; i < PROJECT_SERVICE.getSize(USER_ID_1); i++)
            Assert.assertEquals(PROJECT_SERVICE.findAll(USER_ID_1).get(i), PROJECT_SERVICE.findOneByIndex(USER_ID_1, i));
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll();
        Assert.assertEquals(PROJECT_LIST, actualProjectList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(USER_ID_1);
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedProjectList, actualProjectList);
    }

    @Test
    public void testFindAllForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll((String) null));
    }

    @Test
    public void testFindAllWithSort() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll();

        actualProjectList.get(0).setStatus(Status.IN_PROGRESS);
        actualProjectList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(Sort.BY_STATUS).get(1).getStatus());

        actualProjectList.get(3).setName("A");
        actualProjectList.get(1).setName("B");
        Assert.assertEquals("A", PROJECT_SERVICE.findAll(Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(Sort.BY_NAME).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualProjectList.get(2).setCreated(date1);
        actualProjectList.get(3).setCreated(date2);
        Assert.assertEquals(date1, PROJECT_SERVICE.findAll(Sort.BY_CREATED).get(0).getCreated());
        Assert.assertEquals(date2, PROJECT_SERVICE.findAll(Sort.BY_CREATED).get(1).getCreated());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll();
        Assert.assertEquals(expectedProjectList, PROJECT_SERVICE.findAll((Sort) null));
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(USER_ID_1);

        actualProjectList.get(0).setStatus(Status.IN_PROGRESS);
        actualProjectList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(USER_ID_1, Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(USER_ID_1, Sort.BY_STATUS).get(1).getStatus());

        actualProjectList.get(3).setName("A");
        actualProjectList.get(1).setName("B");
        Assert.assertEquals("A", PROJECT_SERVICE.findAll(USER_ID_1, Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(USER_ID_1, Sort.BY_NAME).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualProjectList.get(2).setCreated(date1);
        actualProjectList.get(3).setCreated(date2);
        Assert.assertEquals(date1, PROJECT_SERVICE.findAll(USER_ID_1, Sort.BY_CREATED).get(0).getCreated());
        Assert.assertEquals(date2, PROJECT_SERVICE.findAll(USER_ID_1, Sort.BY_CREATED).get(1).getCreated());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(expectedProjectList, PROJECT_SERVICE.findAll(USER_ID_1, (Sort) null));
    }

    @Test
    public void testFindAllWithSortForUserForUser() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(null, Sort.BY_CREATED));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparator() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll();
        @Nullable final Comparator<Project> comparatorByCreated = (Comparator<Project>) Sort.BY_CREATED.getComparator();
        @Nullable final Comparator<Project> comparatorByName = (Comparator<Project>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Project> comparatorByStatus = (Comparator<Project>) Sort.BY_STATUS.getComparator();

        actualProjectList.get(0).setStatus(Status.IN_PROGRESS);
        actualProjectList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(comparatorByStatus).get(1).getStatus());

        actualProjectList.get(3).setName("A");
        actualProjectList.get(1).setName("B");
        Assert.assertEquals("A", PROJECT_SERVICE.findAll(comparatorByName).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(comparatorByName).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualProjectList.get(2).setCreated(date1);
        actualProjectList.get(3).setCreated(date2);
        Assert.assertEquals(date1, PROJECT_SERVICE.findAll(comparatorByCreated).get(0).getCreated());
        Assert.assertEquals(date2, PROJECT_SERVICE.findAll(comparatorByCreated).get(1).getCreated());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll();
        Assert.assertEquals(expectedProjectList, PROJECT_SERVICE.findAll((Comparator<Project>) null));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(USER_ID_1);
        @Nullable final Comparator<Project> comparatorByCreated = (Comparator<Project>) Sort.BY_CREATED.getComparator();
        @Nullable final Comparator<Project> comparatorByName = (Comparator<Project>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Project> comparatorByStatus = (Comparator<Project>) Sort.BY_STATUS.getComparator();

        actualProjectList.get(0).setStatus(Status.IN_PROGRESS);
        actualProjectList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(USER_ID_1, comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(USER_ID_1, comparatorByStatus).get(1).getStatus());

        actualProjectList.get(3).setName("A");
        actualProjectList.get(1).setName("B");
        Assert.assertEquals("A", PROJECT_SERVICE.findAll(USER_ID_1, comparatorByName).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(USER_ID_1, comparatorByName).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualProjectList.get(2).setCreated(date1);
        actualProjectList.get(3).setCreated(date2);
        Assert.assertEquals(date1, PROJECT_SERVICE.findAll(USER_ID_1, comparatorByCreated).get(0).getCreated());
        Assert.assertEquals(date2, PROJECT_SERVICE.findAll(USER_ID_1, comparatorByCreated).get(1).getCreated());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(expectedProjectList, PROJECT_SERVICE.findAll(USER_ID_1, (Comparator<Project>) null));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUserNegative() {
        @Nullable final Comparator<Project> comparatorByCreated = (Comparator<Project>) Sort.BY_CREATED.getComparator();
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(null, comparatorByCreated));
    }

    @Test
    public void testCrateProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull final Project project1 = PROJECT_SERVICE.create(USER_ID_1, "NAME1", "DESCRIPTION");
        @NotNull final Project project2 = PROJECT_SERVICE.create(USER_ID_2, "NAME2");

        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertEquals("NAME1", project1.getName());
        Assert.assertEquals("DESCRIPTION", project1.getDescription());
        Assert.assertEquals(USER_ID_1, project1.getUserId());

        Assert.assertEquals("NAME2", project2.getName());
        Assert.assertEquals("", project2.getDescription());
        Assert.assertEquals(USER_ID_2, project2.getUserId());
    }

    @Test
    public void testCrateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, UNKNOWN_ID, null));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final Project project = PROJECT_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusById(USER_ID_1, project.getId(), Status.COMPLETED));
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusById(USER_ID_1, project.getId(), null));
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(null, UNKNOWN_ID, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(UNKNOWN_ID, null, Status.COMPLETED));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.changeProjectStatusById(UNKNOWN_ID, UNKNOWN_ID, Status.COMPLETED));
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final Project project = PROJECT_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusByIndex(USER_ID_1, 0, Status.COMPLETED));
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusByIndex(USER_ID_1, 0, null));
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(null, 0, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(UNKNOWN_ID, -1, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(UNKNOWN_ID, null, Status.COMPLETED));
    }

    @Test
    public void testUpdateById() {
        @NotNull final Project project = PROJECT_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.updateById(USER_ID_1, project.getId(), "NEW N", "NEW D"));
        Assert.assertEquals("NEW N", project.getName());
        Assert.assertEquals("NEW D", project.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final Project project = PROJECT_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.updateByIndex(USER_ID_1, 0, "NEW N", "NEW D"));
        Assert.assertEquals("NEW N", project.getName());
        Assert.assertEquals("NEW D", project.getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateByIndex(null, 0, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(UNKNOWN_ID, -1, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(UNKNOWN_ID, null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES, UNKNOWN_STRING, UNKNOWN_STRING));
    }

}