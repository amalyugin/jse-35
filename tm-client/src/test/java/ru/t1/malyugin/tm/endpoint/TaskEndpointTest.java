package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IUserEndpoint;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.ITokenService;
import ru.t1.malyugin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.malyugin.tm.dto.request.task.*;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserRegistryRequest;
import ru.t1.malyugin.tm.dto.request.user.UserRemoveRequest;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.SoapCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.service.TokenService;

import java.util.ArrayList;
import java.util.List;

@Category(SoapCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final String SOAP_USER_LOGIN = "soap_tst_user";

    @NotNull
    private static final String SOAP_USER_PASS = "soap_pass";

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final ITaskEndpoint TASK_ENDPOINT = ITaskEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final List<Task> TASK_LIST = new ArrayList<>();

    @BeforeClass
    public static void setConnectionAsUsualUser() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest(SOAP_USER_LOGIN, SOAP_USER_PASS, null);
        @Nullable final String userToken = AUTH_ENDPOINT.registry(registryRequest).getToken();
        Assert.assertNotNull(userToken);
        TOKEN_SERVICE.setToken(userToken);
    }

    @NotNull
    private String getUserToken() {
        return TOKEN_SERVICE.getToken();
    }

    @Before
    public void initTest() {
        final int numberOfProjects = 3;
        for (int i = 1; i <= numberOfProjects; i++) {
            @NotNull final TaskCreateRequest request = new TaskCreateRequest(
                    getUserToken(),
                    "NAME " + i,
                    "D " + i
            );
            @Nullable final Task task = TASK_ENDPOINT.creteTask(request).getTask();
            Assert.assertNotNull(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void after() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(getUserToken());
        Assert.assertNotNull(TASK_ENDPOINT.clearTask(request));
        TASK_LIST.clear();
    }

    @AfterClass
    public static void clearUser() {
        @NotNull final String login = PROPERTY_SERVICE.getTestAdminLogin();
        @NotNull final String pass = PROPERTY_SERVICE.getTestAdminPassword();
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest(login, pass);
        @Nullable final String token = AUTH_ENDPOINT.login(requestLogin).getToken();
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(token, SOAP_USER_LOGIN);
        USER_ENDPOINT.removeUser(removeRequest);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                    getUserToken(),
                    task.getId(),
                    status
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.changeTaskStatusById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(status, actualTask.getStatus());
        }
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Status status = Status.COMPLETED;
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                    getUserToken(),
                    TASK_LIST.indexOf(task),
                    status
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.changeTaskStatusByIndex(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(status, actualTask.getStatus());
        }
    }

    @Test
    public void testCompleteById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(
                    getUserToken(),
                    task.getId()
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.completeTaskById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
        }
    }

    @Test
    public void testCompleteByIndex() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(
                    getUserToken(),
                    TASK_LIST.indexOf(task)
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.completeTaskByIndex(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
        }
    }

    @Test
    public void testStartById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(
                    getUserToken(), task.getId()
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.startTaskById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
        }
    }

    @Test
    public void testStartByIndex() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(
                    getUserToken(), TASK_LIST.indexOf(task)
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.startTaskByIndex(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(
                    getUserToken(), task.getId()
            );
            TASK_ENDPOINT.removeTaskById(request);
            @NotNull final TaskShowByIdRequest requestTask = new TaskShowByIdRequest(
                    getUserToken(), task.getId());
            Assert.assertNull(TASK_ENDPOINT.showTaskById(requestTask).getTask());
        }
    }

    @Test
    public void testRemoveByIndex() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(
                    getUserToken(),
                    0
            );
            TASK_ENDPOINT.removeTaskByIndex(request);
            @NotNull final TaskShowByIdRequest requestTask = new TaskShowByIdRequest(
                    getUserToken(), task.getId());
            Assert.assertNull(TASK_ENDPOINT.showTaskById(requestTask).getTask());
        }
    }

    @Test
    public void testShowById() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(
                    getUserToken(), task.getId()
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(task.getId(), actualTask.getId());
        }
    }

    @Test
    public void testShowByIndex() {
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(
                    getUserToken(), TASK_LIST.indexOf(task)
            );
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskByIndex(request).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(task.getId(), actualTask.getId());
        }
    }

    @Test
    public void testShowList() {
        @NotNull final TaskShowListRequest request = new TaskShowListRequest(getUserToken(), null);
        @Nullable List<Task> actualList = TASK_ENDPOINT.showTaskList(request).getTaskList();
        Assert.assertNotNull(actualList);
        Assert.assertEquals(TASK_LIST.size(), actualList.size());
    }

    @Test
    public void testShowListByProjectId() {
        @NotNull final String projectName = "P1";
        @NotNull final String projectDescription = "D";

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                getUserToken(), projectName, projectDescription
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(requestCreateProject).getProject();
        Assert.assertNotNull(project);
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(requestBind));
        }
        @NotNull final TaskShowListByProjectIdRequest request = new TaskShowListByProjectIdRequest(
                getUserToken(), project.getId()
        );
        @Nullable final List<Task> actualList = TASK_ENDPOINT.showTaskListByProject(request).getTaskList();
        Assert.assertNotNull(actualList);
        Assert.assertEquals(TASK_LIST.size(), actualList.size());
    }

    @Test
    public void testBindTask() {
        @NotNull final String projectName = "P1";
        @NotNull final String projectDescription = "D";
        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                getUserToken(), projectName, projectDescription
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(requestCreateProject).getProject();
        Assert.assertNotNull(project);
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(request));

            @NotNull final TaskShowByIdRequest requestActualTask = new TaskShowByIdRequest(getUserToken(), task.getId());
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestActualTask).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertNotNull(actualTask.getProjectId());
            @NotNull final String actualTaskProjectId = actualTask.getProjectId();
            Assert.assertEquals(project.getId(), actualTaskProjectId);
        }
    }

    @Test
    public void testUnbindTask() {
        @NotNull final String projectName = "P1";
        @NotNull final String projectDescription = "D";
        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                getUserToken(), projectName, projectDescription
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(requestCreateProject).getProject();
        Assert.assertNotNull(project);
        for (@NotNull final Task task : TASK_LIST) {
            @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.bindTaskToProject(request));

            @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(
                    getUserToken(), task.getId(), project.getId()
            );
            Assert.assertNotNull(TASK_ENDPOINT.unbindTaskFromProject(requestUnbind));
            @NotNull final TaskShowByIdRequest requestActualTask = new TaskShowByIdRequest(getUserToken(), task.getId());
            @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestActualTask).getTask();
            Assert.assertNotNull(actualTask);
            Assert.assertNull(actualTask.getProjectId());
        }
    }

    @Test
    public void testClear() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(getUserToken());
        Assert.assertNotNull(TASK_ENDPOINT.clearTask(request));
        @NotNull final TaskShowListRequest requestList = new TaskShowListRequest(getUserToken(), null);
        Assert.assertNull(TASK_ENDPOINT.showTaskList(requestList).getTaskList());
    }

    @Test
    public void testCreate() {
        @NotNull final String name = "TEST_NAME";
        @NotNull final String description = "TEST_DESC";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(
                getUserToken(), name, description
        );
        @Nullable final Task task = TASK_ENDPOINT.creteTask(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String name = "NEW_NAME";
        @NotNull final String description = "NEW_DESCRIPTION";
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(
                getUserToken(), task.getId(), name, description);
        Assert.assertNotNull(TASK_ENDPOINT.updateTaskById(request));

        @NotNull final TaskShowByIdRequest requestTask = new TaskShowByIdRequest(
                getUserToken(), task.getId());
        @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestTask).getTask();
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "NEW_NAME";
        @NotNull final String description = "NEW_DESCRIPTION";
        final int index = 0;
        @NotNull final Task task = TASK_LIST.get(index);
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(
                getUserToken(), index, name, description);
        Assert.assertNotNull(TASK_ENDPOINT.updateTaskByIndex(request));

        @NotNull final TaskShowByIdRequest requestTask = new TaskShowByIdRequest(
                getUserToken(), task.getId());
        @Nullable final Task actualTask = TASK_ENDPOINT.showTaskById(requestTask).getTask();
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

}