package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    private static final List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project N " + i);
            project.setDescription("Desc " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        PROJECT_REPOSITORY.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        PROJECT_REPOSITORY.add(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testAddProjectForUser() {
        final int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_1) + 1;
        @NotNull final Project project = new Project();
        PROJECT_REPOSITORY.add(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID_2, project.getId()));
    }

    @Test
    public void testCreateProject() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "TEST Project";
        @NotNull final String description = "TEST Description";
        @NotNull final Project project1 = PROJECT_REPOSITORY.create(USER_ID_1, name, description);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertEquals(name, project1.getName());
        Assert.assertEquals(description, project1.getDescription());
        Assert.assertEquals(USER_ID_1, project1.getUserId());

        expectedNumberOfEntries++;
        @NotNull final Project project2 = PROJECT_REPOSITORY.create(USER_ID_2, name);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertEquals(name, project2.getName());
        Assert.assertEquals("", project2.getDescription());
        Assert.assertEquals(USER_ID_2, project2.getUserId());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projectList.add(new Project());
        }
        PROJECT_REPOSITORY.addAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        PROJECT_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testClearForUser() {
        final int expectedNumberOfEntries = 0;

        PROJECT_REPOSITORY.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_ENTRIES, PROJECT_REPOSITORY.getSize());

        PROJECT_REPOSITORY.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_2));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projectList = PROJECT_REPOSITORY.findAll();
        Assert.assertEquals(PROJECT_LIST, projectList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_REPOSITORY.findAll(USER_ID_1);
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedProjectList, actualProjectList);
    }

    @Test
    public void testFindOneById() {
        for (@NotNull final Project project : PROJECT_LIST) {
            Assert.assertEquals(project, PROJECT_REPOSITORY.findOneById(project.getId()));
        }
        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final List<Project> userProjectList = PROJECT_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (@NotNull final Project project : userProjectList) {
            Assert.assertEquals(project, PROJECT_REPOSITORY.findOneById(USER_ID_1, project.getId()));
            Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID_2, project.getId()));
        }

        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID_1, id));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < PROJECT_REPOSITORY.getSize(); i++) {
            Assert.assertEquals(PROJECT_LIST.get(i), PROJECT_REPOSITORY.findOneByIndex(i));
        }

        Assert.assertNull(PROJECT_REPOSITORY.findOneByIndex(-1));
        Assert.assertNull(PROJECT_REPOSITORY.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Project> userProjectList = PROJECT_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userProjectList.size(); i++) {
            Assert.assertEquals(userProjectList.get(i), PROJECT_REPOSITORY.findOneByIndex(USER_ID_1, i));
            Assert.assertNotEquals(userProjectList.get(i), PROJECT_REPOSITORY.findOneByIndex(USER_ID_2, i));
        }

        Assert.assertNull(PROJECT_REPOSITORY.findOneByIndex(USER_ID_1, -1));
        Assert.assertNull(PROJECT_REPOSITORY.findOneByIndex(USER_ID_1, userProjectList.size() + 1));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final int userProjectListSize = (int) PROJECT_LIST.stream().filter(p -> USER_ID_1.equals(p.getUserId())).count();
        Assert.assertEquals(userProjectListSize, PROJECT_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testSet() {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projectList.add(new Project());
        }
        PROJECT_REPOSITORY.set(projectList);
        Assert.assertEquals(projectList, PROJECT_REPOSITORY.findAll());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project.getId()));
    }

    @Test
    public void testRemoveForUser() {
        int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Project project1 = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0);

        PROJECT_REPOSITORY.remove(USER_ID_1, project1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(USER_ID_1, project1.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, PROJECT_REPOSITORY.getSize());

        expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_2);
        @NotNull final Project project2 = PROJECT_REPOSITORY.findAll(USER_ID_2).get(0);

        PROJECT_REPOSITORY.remove(USER_ID_1, project2);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.removeById(project.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Project project1 = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0);

        PROJECT_REPOSITORY.removeById(USER_ID_1, project1.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project1.getId()));

        expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_2);
        @NotNull final Project project2 = PROJECT_REPOSITORY.findAll(USER_ID_2).get(0);

        PROJECT_REPOSITORY.removeById(USER_ID_1, project2.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_2));

        expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        PROJECT_REPOSITORY.removeByIndex(index);
        Assert.assertNull(PROJECT_REPOSITORY.removeByIndex(-1));
        Assert.assertNull(PROJECT_REPOSITORY.removeByIndex(NUMBER_OF_ENTRIES + 1));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Project project = PROJECT_REPOSITORY.findAll(USER_ID_1).get(0);

        int index = PROJECT_LIST.indexOf(project);

        Assert.assertNotNull(PROJECT_REPOSITORY.removeByIndex(USER_ID_1, index));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project.getId()));
        Assert.assertNull(PROJECT_REPOSITORY.removeByIndex(USER_ID_1, -1));
        Assert.assertNull(PROJECT_REPOSITORY.removeByIndex(USER_ID_1, PROJECT_REPOSITORY.getSize(USER_ID_1) + 1));
        expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

}