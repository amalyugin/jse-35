package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IUserEndpoint;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.ITokenService;
import ru.t1.malyugin.tm.dto.request.project.*;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserRegistryRequest;
import ru.t1.malyugin.tm.dto.request.user.UserRemoveRequest;
import ru.t1.malyugin.tm.dto.response.project.ProjectShowListResponse;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.SoapCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.service.TokenService;

import java.util.ArrayList;
import java.util.List;

@Category(SoapCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final String SOAP_USER_LOGIN = "soap_tst_user";

    @NotNull
    private static final String SOAP_USER_PASS = "soap_pass";

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final List<Project> projectList = new ArrayList<>();

    @BeforeClass
    public static void setConnectionAsUsualUser() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest(SOAP_USER_LOGIN, SOAP_USER_PASS, null);
        @Nullable final String userToken = AUTH_ENDPOINT.registry(registryRequest).getToken();
        Assert.assertNotNull(userToken);
        TOKEN_SERVICE.setToken(userToken);
    }

    @NotNull
    private String getUserToken() {
        return TOKEN_SERVICE.getToken();
    }

    @Before
    public void initTest() {
        final int numberOfProjects = 3;
        for (int i = 1; i <= numberOfProjects; i++) {
            @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                    getUserToken(),
                    "NAME " + i,
                    "D " + i
            );
            @Nullable final Project project = PROJECT_ENDPOINT.creteProject(request).getProject();
            Assert.assertNotNull(project);
            projectList.add(project);
        }
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getUserToken());
        Assert.assertNotNull(PROJECT_ENDPOINT.clearProject(request));
    }

    @AfterClass
    public static void clearUser() {
        @NotNull final String login = PROPERTY_SERVICE.getTestAdminLogin();
        @NotNull final String pass = PROPERTY_SERVICE.getTestAdminPassword();
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest(login, pass);
        @Nullable final String token = AUTH_ENDPOINT.login(requestLogin).getToken();
        Assert.assertNotNull(token);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(token, SOAP_USER_LOGIN);
        USER_ENDPOINT.removeUser(removeRequest);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                    getUserToken(),
                    project.getId(),
                    status
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.changeProjectStatusById(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(status, actualProject.getStatus());
        }
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Status status = Status.COMPLETED;
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                    getUserToken(),
                    projectList.indexOf(project),
                    status
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.changeProjectStatusByIndex(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(status, actualProject.getStatus());
        }
    }

    @Test
    public void testCompleteById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(
                    getUserToken(),
                    project.getId()
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.completeProjectById(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
        }
    }

    @Test
    public void testCompleteByIndex() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(
                    getUserToken(),
                    projectList.indexOf(project)
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.completeProjectByIndex(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
        }
    }

    @Test
    public void testStartById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(
                    getUserToken(), project.getId()
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.startProjectById(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
        }
    }

    @Test
    public void testStartByIndex() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(
                    getUserToken(), projectList.indexOf(project)
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.startProjectByIndex(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(
                    getUserToken(), project.getId()
            );
            PROJECT_ENDPOINT.removeProjectById(request);
            @NotNull final ProjectShowByIdRequest requestProject = new ProjectShowByIdRequest(
                    getUserToken(), project.getId());
            Assert.assertNull(PROJECT_ENDPOINT.showProjectById(requestProject).getProject());
        }
    }

    @Test
    public void testRemoveByIndex() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(
                    getUserToken(),
                    0
            );
            PROJECT_ENDPOINT.removeProjectByIndex(request);
            @NotNull final ProjectShowByIdRequest requestProject = new ProjectShowByIdRequest(
                    getUserToken(), project.getId());
            Assert.assertNull(PROJECT_ENDPOINT.showProjectById(requestProject).getProject());
        }
    }

    @Test
    public void testShowById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(
                    getUserToken(), project.getId()
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.showProjectById(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(project.getId(), actualProject.getId());
        }
    }

    @Test
    public void testShowByIndex() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(
                    getUserToken(), projectList.indexOf(project)
            );
            @Nullable final Project actualProject = PROJECT_ENDPOINT.showProjectByIndex(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(project.getId(), actualProject.getId());
        }
    }

    @Test
    public void testShowList() {
        @NotNull final ProjectShowListRequest request = new ProjectShowListRequest(getUserToken(), null);
        @Nullable final ProjectShowListResponse response = PROJECT_ENDPOINT.showProjectList(request);
        Assert.assertNotNull(response.getProjectList());
        @NotNull List<Project> actualList = response.getProjectList();
        Assert.assertEquals(projectList.size(), actualList.size());
    }

    @Test
    public void testClear() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getUserToken());
        Assert.assertNotNull(PROJECT_ENDPOINT.clearProject(request));
        @NotNull final ProjectShowListRequest requestList = new ProjectShowListRequest(getUserToken(), null);
        Assert.assertNull(PROJECT_ENDPOINT.showProjectList(requestList).getProjectList());
    }

    @Test
    public void testCreate() {
        @NotNull final String name = "TEST_NAME";
        @NotNull final String description = "TEST_DESC";
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                getUserToken(), name, description
        );
        @Nullable final Project project = PROJECT_ENDPOINT.creteProject(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String name = "NEW_NAME";
        @NotNull final String description = "NEW_DESCRIPTION";
        @NotNull final Project project = projectList.get(0);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                getUserToken(), project.getId(), name, description);
        Assert.assertNotNull(PROJECT_ENDPOINT.updateProjectById(request));

        @NotNull final ProjectShowByIdRequest requestProject = new ProjectShowByIdRequest(
                getUserToken(), project.getId());
        @Nullable final Project actualProject = PROJECT_ENDPOINT.showProjectById(requestProject).getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "NEW_NAME";
        @NotNull final String description = "NEW_DESCRIPTION";
        final int index = 0;
        @NotNull final Project project = projectList.get(index);
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                getUserToken(), index, name, description);
        Assert.assertNotNull(PROJECT_ENDPOINT.updateProjectByIndex(request));

        @NotNull final ProjectShowByIdRequest requestProject = new ProjectShowByIdRequest(
                getUserToken(), project.getId());
        @Nullable final Project actualProject = PROJECT_ENDPOINT.showProjectById(requestProject).getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

}