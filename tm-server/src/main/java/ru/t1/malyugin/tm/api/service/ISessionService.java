package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}