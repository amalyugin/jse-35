package ru.t1.malyugin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    int getSize();

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> addAll(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(@NotNull List<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

}