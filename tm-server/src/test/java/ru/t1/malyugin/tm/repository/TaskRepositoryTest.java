package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class TaskRepositoryTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    private static final List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task N " + i);
            task.setDescription("Desc " + i);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            TASK_REPOSITORY.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_REPOSITORY.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testAddTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        TASK_REPOSITORY.add(task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testAddTaskForUser() {
        final int expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_1) + 1;
        @NotNull final Task task = new Task();
        TASK_REPOSITORY.add(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.findOneById(USER_ID_2, task.getId()));
    }

    @Test
    public void testCreateTask() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "TEST Task";
        @NotNull final String description = "TEST Description";
        @NotNull final Task task1 = TASK_REPOSITORY.create(USER_ID_1, name, description);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertEquals(name, task1.getName());
        Assert.assertEquals(description, task1.getDescription());
        Assert.assertEquals(USER_ID_1, task1.getUserId());

        expectedNumberOfEntries++;
        @NotNull final Task task2 = TASK_REPOSITORY.create(USER_ID_2, name);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertEquals(name, task2.getName());
        Assert.assertEquals("", task2.getDescription());
        Assert.assertEquals(USER_ID_2, task2.getUserId());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            taskList.add(new Task());
        }
        TASK_REPOSITORY.addAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        TASK_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testClearForUser() {
        final int expectedNumberOfEntries = 0;

        TASK_REPOSITORY.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_ENTRIES, TASK_REPOSITORY.getSize());

        TASK_REPOSITORY.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNotEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_2));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> taskList = TASK_REPOSITORY.findAll();
        Assert.assertEquals(TASK_LIST, taskList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> actualTaskList = TASK_REPOSITORY.findAll(USER_ID_1);
        @NotNull final List<Task> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList, actualTaskList);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> actualTaskList = TASK_REPOSITORY.findAll(USER_ID_1);
        @NotNull final String tempProjectId = "TEMP_PROJECT_ID";
        actualTaskList.get(0).setProjectId(tempProjectId);
        actualTaskList.get(1).setProjectId(tempProjectId);
        actualTaskList.get(2).setProjectId(tempProjectId);

        @NotNull final List<Task> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .filter(p -> tempProjectId.equals(p.getProjectId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList, TASK_REPOSITORY.findAllByProjectId(USER_ID_1, tempProjectId));
        Assert.assertEquals(Collections.EMPTY_LIST, TASK_REPOSITORY.findAllByProjectId(USER_ID_2, tempProjectId));
    }

    @Test
    public void testFindOneById() {
        for (@NotNull final Task task : TASK_LIST) {
            Assert.assertEquals(task, TASK_REPOSITORY.findOneById(task.getId()));
        }
        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(TASK_REPOSITORY.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final List<Task> userTaskList = TASK_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (@NotNull final Task task : userTaskList) {
            Assert.assertEquals(task, TASK_REPOSITORY.findOneById(USER_ID_1, task.getId()));
            Assert.assertNull(TASK_REPOSITORY.findOneById(USER_ID_2, task.getId()));
        }

        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(TASK_REPOSITORY.findOneById(USER_ID_1, id));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < TASK_REPOSITORY.getSize(); i++) {
            Assert.assertEquals(TASK_LIST.get(i), TASK_REPOSITORY.findOneByIndex(i));
        }

        Assert.assertNull(TASK_REPOSITORY.findOneByIndex(-1));
        Assert.assertNull(TASK_REPOSITORY.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Task> userTaskList = TASK_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userTaskList.size(); i++) {
            Assert.assertEquals(userTaskList.get(i), TASK_REPOSITORY.findOneByIndex(USER_ID_1, i));
            Assert.assertNotEquals(userTaskList.get(i), TASK_REPOSITORY.findOneByIndex(USER_ID_2, i));
        }

        Assert.assertNull(TASK_REPOSITORY.findOneByIndex(USER_ID_1, -1));
        Assert.assertNull(TASK_REPOSITORY.findOneByIndex(USER_ID_1, userTaskList.size() + 1));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(TASK_LIST.size(), TASK_REPOSITORY.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final int userTaskListSize = (int) TASK_LIST.stream().filter(p -> USER_ID_1.equals(p.getUserId())).count();
        Assert.assertEquals(userTaskListSize, TASK_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testSet() {
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            taskList.add(new Task());
        }
        TASK_REPOSITORY.set(taskList);
        Assert.assertEquals(taskList, TASK_REPOSITORY.findAll());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_REPOSITORY.remove(task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertNull(TASK_REPOSITORY.findOneById(task.getId()));
    }

    @Test
    public void testRemoveForUser() {
        int expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Task task1 = TASK_REPOSITORY.findAll(USER_ID_1).get(0);

        TASK_REPOSITORY.remove(USER_ID_1, task1);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.findOneById(USER_ID_1, task1.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, TASK_REPOSITORY.getSize());

        expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_2);
        @NotNull final Task task2 = TASK_REPOSITORY.findAll(USER_ID_2).get(0);

        TASK_REPOSITORY.remove(USER_ID_1, task2);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_REPOSITORY.removeById(task.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertNull(TASK_REPOSITORY.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        int expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Task task1 = TASK_REPOSITORY.findAll(USER_ID_1).get(0);

        TASK_REPOSITORY.removeById(USER_ID_1, task1.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.findOneById(task1.getId()));

        expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_2);
        @NotNull final Task task2 = TASK_REPOSITORY.findAll(USER_ID_2).get(0);

        TASK_REPOSITORY.removeById(USER_ID_1, task2.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_2));

        expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        TASK_REPOSITORY.removeByIndex(index);
        Assert.assertNull(TASK_REPOSITORY.removeByIndex(-1));
        Assert.assertNull(TASK_REPOSITORY.removeByIndex(NUMBER_OF_ENTRIES + 1));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        int expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Task task = TASK_REPOSITORY.findAll(USER_ID_1).get(0);
        int index = TASK_LIST.indexOf(task);

        Assert.assertNotNull(TASK_REPOSITORY.removeByIndex(USER_ID_1, index));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.findOneById(task.getId()));
        Assert.assertNull(TASK_REPOSITORY.removeByIndex(USER_ID_1, -1));
        Assert.assertNull(TASK_REPOSITORY.removeByIndex(USER_ID_1, TASK_REPOSITORY.getSize(USER_ID_1) + 1));
        expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

}