package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.ITokenService;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.marker.SoapCategory;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.service.TokenService;

@Category(SoapCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private static final ITokenService TOKEN_SERVICE_ADMIN = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IDomainEndpoint DOMAIN_ENDPOINT = IDomainEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static String getAdminToken() {
        return TOKEN_SERVICE_ADMIN.getToken();
    }

    @BeforeClass
    public static void setConnection() {
        @NotNull final String adminLogin = PROPERTY_SERVICE.getTestAdminLogin();
        @NotNull final String adminPass = PROPERTY_SERVICE.getTestAdminPassword();
        @NotNull final UserLoginRequest loginRequestAdmin = new UserLoginRequest(adminLogin, adminPass);
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequestAdmin).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE_ADMIN.setToken(adminToken);
    }

    @Test
    public void testSaveBackup() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveBackup(request));
    }

    @Test
    public void testLoadBackup() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadBackup(request));
    }

    @Test
    public void testSaveBase64() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveBase64(request));
    }

    @Test
    public void testLoadBase64() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadBase64(request));
    }

    @Test
    public void testSaveBinary() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveBinary(request));
    }

    @Test
    public void testLoadBinary() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadBinary(request));
    }

    @Test
    public void testSaveJsonFasterXml() {
        @NotNull final DataJsonFasterXmlSaveRequest request = new DataJsonFasterXmlSaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveJsonFasterXml(request));
    }

    @Test
    public void testLoadJsonFasterXml() {
        @NotNull final DataJsonFasterXmlLoadRequest request = new DataJsonFasterXmlLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadJsonFasterXml(request));
    }

    @Test
    public void testSaveXmlFasterXml() {
        @NotNull final DataXmlFasterXmlSaveRequest request = new DataXmlFasterXmlSaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveXmlFasterXml(request));
    }

    @Test
    public void testLoadXmlFasterXml() {
        @NotNull final DataXmlFasterXmlLoadRequest request = new DataXmlFasterXmlLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadXmlFasterXml(request));
    }

    @Test
    public void testSaveJsonJaxB() {
        @NotNull final DataJsonJaxBSaveRequest request = new DataJsonJaxBSaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveJsonJaxB(request));
    }

    @Test
    public void testLoadJsonJaxB() {
        @NotNull final DataJsonJaxBLoadRequest request = new DataJsonJaxBLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadJsonJaxB(request));
    }

    @Test
    public void testSaveXmlJaxB() {
        @NotNull final DataXmlJaxBSaveRequest request = new DataXmlJaxBSaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveXmlJaxB(request));
    }

    @Test
    public void testLoadXmlJaxB() {
        @NotNull final DataXmlJaxBLoadRequest request = new DataXmlJaxBLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadXmlJaxB(request));
    }

    @Test
    public void testSaveYaml() {
        @NotNull final DataYamlFasterXmlSaveRequest request = new DataYamlFasterXmlSaveRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveYaml(request));
    }

    @Test
    public void testLoadYaml() {
        @NotNull final DataYamlFasterXmlLoadRequest request = new DataYamlFasterXmlLoadRequest(getAdminToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadYaml(request));
    }

}