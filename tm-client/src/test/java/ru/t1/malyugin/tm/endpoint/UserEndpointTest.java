package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IUserEndpoint;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.ITokenService;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.marker.SoapCategory;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.service.TokenService;

@Category(SoapCategory.class)
public final class UserEndpointTest {

    @NotNull
    private static final String SOAP_USER_LOGIN = "soap_tst_user";

    @NotNull
    private static final String SOAP_USER_PASS = "soap_pass";

    @NotNull
    private static final ITokenService TOKEN_SERVICE_ADMIN = new TokenService();

    @NotNull
    private static final ITokenService TOKEN_SERVICE_USER = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static String getUserToken() {
        return TOKEN_SERVICE_USER.getToken();
    }

    @NotNull
    private static String getAdminToken() {
        return TOKEN_SERVICE_ADMIN.getToken();
    }


    @BeforeClass
    public static void setConnection() {
        @NotNull final String adminLogin = PROPERTY_SERVICE.getTestAdminLogin();
        @NotNull final String adminPass = PROPERTY_SERVICE.getTestAdminPassword();
        @NotNull final UserLoginRequest requestAdmin = new UserLoginRequest(adminLogin, adminPass);
        @Nullable final String adminToken = AUTH_ENDPOINT.login(requestAdmin).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE_ADMIN.setToken(adminToken);

        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest(SOAP_USER_LOGIN, SOAP_USER_PASS, "SOAP@TEST.ru");
        @Nullable final String userToken = AUTH_ENDPOINT.registry(registryRequest).getToken();
        Assert.assertNotNull(userToken);
        TOKEN_SERVICE_USER.setToken(userToken);
    }

    @AfterClass
    public static void clearUser() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getAdminToken(), SOAP_USER_LOGIN);
        USER_ENDPOINT.removeUser(request);
    }

    @Test
    public void testUserLockUnlock() {
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(getAdminToken(), SOAP_USER_LOGIN);
        Assert.assertNotNull(USER_ENDPOINT.lockUser(lockRequest));
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest(SOAP_USER_LOGIN, SOAP_USER_PASS);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(requestLogin));
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(getAdminToken(), SOAP_USER_LOGIN);
        Assert.assertNotNull(USER_ENDPOINT.unlockUser(unlockRequest).getUser());
        Assert.assertNotNull(AUTH_ENDPOINT.login(requestLogin).getToken());
    }

    @Test
    public void testChangePassword() {
        @NotNull final String newPass = "NEW_PASS";
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(getUserToken(), newPass);
        Assert.assertNotNull(USER_ENDPOINT.changePassword(changePasswordRequest).getUser());
        @NotNull final UserLoginRequest loginRequestError = new UserLoginRequest(SOAP_USER_LOGIN, SOAP_USER_PASS);
        @NotNull final UserLoginRequest loginRequestSuccess = new UserLoginRequest(SOAP_USER_LOGIN, newPass);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(loginRequestError));
        Assert.assertNotNull(AUTH_ENDPOINT.login(loginRequestSuccess).getToken());
        @NotNull final UserChangePasswordRequest changePasswordRequestBack = new UserChangePasswordRequest(getUserToken(), SOAP_USER_PASS);
        Assert.assertNotNull(USER_ENDPOINT.changePassword(changePasswordRequestBack).getUser());
    }

    @Test
    public void testLogin() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(SOAP_USER_LOGIN, SOAP_USER_PASS);
        Assert.assertNotNull(AUTH_ENDPOINT.login(loginRequest).getToken());

        @NotNull final String wrongPass = "WRONG";
        @NotNull final UserLoginRequest loginRequestError = new UserLoginRequest(SOAP_USER_LOGIN, wrongPass);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(loginRequestError));
    }

    @Test
    public void testRegistry() {
        @NotNull final String newUsualLogin = "tst_usr";
        @NotNull final String newUsualPass = "tst_pass";
        @NotNull final String newUsualMail = "tst@mail.ru";
        @NotNull final String newAdminLogin = "adm_usr";
        @NotNull final String newAdminPass = "adm_pass";
        @NotNull final String newAdminMail = "adm@mail.ru";

        @NotNull final UserRegistryRequest registryRequestUser = new UserRegistryRequest(newUsualLogin, newUsualPass, newUsualMail);
        @NotNull final UserRegistryRequest registryRequestAdmin = new UserRegistryRequest(newAdminLogin, newAdminPass, newAdminMail);

        Assert.assertNotNull(AUTH_ENDPOINT.registry(registryRequestUser).getToken());
        Assert.assertNotNull(AUTH_ENDPOINT.registry(registryRequestAdmin).getToken());

        @NotNull final UserRemoveRequest removeRequest1 = new UserRemoveRequest(getAdminToken(), newUsualLogin);
        @NotNull final UserRemoveRequest removeRequest2 = new UserRemoveRequest(getAdminToken(), newAdminLogin);
        Assert.assertNotNull(USER_ENDPOINT.removeUser(removeRequest1).getUser());
        Assert.assertNotNull(USER_ENDPOINT.removeUser(removeRequest2).getUser());
    }

    @Test
    public void testLogout() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getUserToken());
        Assert.assertNotNull(AUTH_ENDPOINT.logout(request));
        @NotNull final String newPass = "tst_pass";
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(getUserToken(), newPass);
        Assert.assertThrows(Exception.class, () -> USER_ENDPOINT.changePassword(changePasswordRequest));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(SOAP_USER_LOGIN, SOAP_USER_PASS);
        @Nullable final String token = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(token);
        TOKEN_SERVICE_USER.setToken(token);
    }

    @Test
    public void testUpdateViewProfile() {
        @NotNull final String firstName = "Иван";
        @NotNull final String middleName = "Иванович";
        @NotNull final String lastName = "Иванов";

        @NotNull final UserUpdateProfileRequest userUpdateProfileRequest = new UserUpdateProfileRequest(
                getUserToken(), firstName, middleName, lastName
        );
        Assert.assertNotNull(USER_ENDPOINT.updateProfile(userUpdateProfileRequest));

        @NotNull final UserGetProfileRequest userGetProfileRequest = new UserGetProfileRequest(getUserToken());
        @Nullable final User profile = USER_ENDPOINT.getProfile(userGetProfileRequest).getUser();
        Assert.assertNotNull(profile);
        Assert.assertEquals(firstName, profile.getFirstName());
        Assert.assertEquals(middleName, profile.getMiddleName());
        Assert.assertEquals(lastName, profile.getLastName());
    }

}