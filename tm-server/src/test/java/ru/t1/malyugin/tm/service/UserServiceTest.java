package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;
import ru.t1.malyugin.tm.repository.UserRepository;
import ru.t1.malyugin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(USER_REPOSITORY, PROJECT_REPOSITORY, TASK_REPOSITORY, PROPERTY_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE_WRONG_PROPERTY = new UserService(USER_REPOSITORY, PROJECT_REPOSITORY, TASK_REPOSITORY, null);

    @NotNull
    private static final List<User> USER_LIST = new ArrayList<>();

    private static final int NUMBER_OF_USERS = 2;

    @NotNull
    private static final String UNKNOWN_STRING = "SOME_STR";

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_USERS; i++) {
            @NotNull final User user = USER_REPOSITORY.create("user" + i, "pass" + i, "email" + i, Role.USUAL);
            USER_REPOSITORY.add(user);
            USER_LIST.add(user);
        }
    }

    @After
    public void clearData() {
        USER_REPOSITORY.clear();
        USER_LIST.clear();
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user, USER_SERVICE.findOneByLogin(user.getLogin()));
        }
        Assert.assertNull(USER_SERVICE.findOneByLogin(null));
    }

    @Test
    public void testFindOneByEmail() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user, USER_SERVICE.findOneByEmail(user.getEmail()));
        }
        Assert.assertNull(USER_SERVICE.findOneByEmail(null));
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(true, USER_SERVICE.isLoginExist(user.getLogin()));
        }
        Assert.assertEquals(false, USER_SERVICE.isLoginExist(null));
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(true, USER_SERVICE.isEmailExist(user.getEmail()));
        }
        Assert.assertEquals(false, USER_SERVICE.isEmailExist(null));
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertNotNull(USER_SERVICE.lockUserByLogin(user.getLogin()));
            Assert.assertEquals(true, user.getLocked());
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.lockUserByLogin(UNKNOWN_STRING));
    }

    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertNotNull(USER_SERVICE.lockUserByLogin(user.getLogin()));
            Assert.assertEquals(true, user.getLocked());
            Assert.assertNotNull(USER_SERVICE.unlockUserByLogin(user.getLogin()));
            Assert.assertEquals(false, user.getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.unlockUserByLogin(UNKNOWN_STRING));
    }

    @Test
    public void testCreateUser() {
        final int expectedNumberOfUsers = NUMBER_OF_USERS + 4;
        @NotNull final User user1 = USER_SERVICE.create("NEW1", "PASS1", "MAIL1");
        @NotNull final User user2 = USER_SERVICE.create("NEW2", "PASS2", "MAIL2", Role.ADMIN);
        @NotNull final User user3 = USER_SERVICE.create("NEW3", "PASS3", null);
        @NotNull final User user4 = USER_SERVICE.create("NEW4", "PASS4", null, null);

        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
        Assert.assertEquals("NEW1", user1.getLogin());
        Assert.assertEquals("MAIL1", user1.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS1"), user1.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user1.getRole());
        Assert.assertEquals("NEW2", user2.getLogin());
        Assert.assertEquals("MAIL2", user2.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS2"), user2.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user2.getRole());
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, Role.USUAL));

        @NotNull final User user = USER_LIST.get(0);
        Assert.assertThrows(LoginExistException.class, () -> USER_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(LoginExistException.class, () -> USER_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(EmailExistException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail()));
        Assert.assertThrows(EmailExistException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), Role.USUAL));

        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE_WRONG_PROPERTY.create("NEW", "PASS", null));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE_WRONG_PROPERTY.create("NEW", "PASS", null, Role.USUAL));
    }

    @Test
    public void testRemoveByLogin() {
        final int expectedNumberOfUsers = NUMBER_OF_USERS - 1;
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertNotNull(USER_SERVICE.removeByLogin(user.getLogin()));
        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
    }

    @Test
    public void testRemoveByEmail() {
        final int expectedNumberOfUsers = NUMBER_OF_USERS - 1;
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertNotNull(USER_SERVICE.removeByEmail(user.getEmail()));
        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(null));
    }

    @Test
    public void testRemove() {
        @NotNull final User user = USER_LIST.get(0);
        final int expectedNumberOfEntries = 0;
        Assert.assertNotNull(USER_SERVICE.remove(user));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(user.getId()));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(user.getId()));
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.remove(null));
    }

    @Test
    public void testSetPassword() {
        @NotNull final User user = USER_LIST.get(0);
        @NotNull final String pass = "NEWP";
        @Nullable final String passHash = HashUtil.salt(PROPERTY_SERVICE, pass);
        Assert.assertNotNull(USER_SERVICE.setPassword(user.getId(), pass));
        Assert.assertEquals(passHash, user.getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, UNKNOWN_STRING));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(UNKNOWN_STRING, null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(UNKNOWN_STRING, UNKNOWN_STRING));

        @NotNull final User user = USER_LIST.get(0);
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE_WRONG_PROPERTY.setPassword(user.getId(), "PASS"));
    }

    @Test
    public void testUpdateProfile() {
        @NotNull final User user = USER_LIST.get(0);
        @NotNull final String test = "TEST";
        Assert.assertNotNull(USER_SERVICE.updateProfile(user.getId(), null, null, test));
        Assert.assertNotNull(USER_SERVICE.updateProfile(user.getId(), null, test, null));
        Assert.assertNotNull(USER_SERVICE.updateProfile(user.getId(), test, null, null));
        Assert.assertEquals(test, user.getFirstName());
        Assert.assertEquals(test, user.getMiddleName());
        Assert.assertEquals(test, user.getLastName());
    }

    @Test
    public void testUpdateProfileNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateProfile(null, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.updateProfile(UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
    }

}