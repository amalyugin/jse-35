package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.TaskRepository;

import java.util.*;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(TASK_REPOSITORY);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID = UUID.randomUUID().toString();

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task("P" + i, "D" + i);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            if (i <= 3) task.setProjectId(PROJECT_ID);
            TASK_REPOSITORY.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_REPOSITORY.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testAdd() {
        Assert.assertNull(TASK_SERVICE.add(null));
        Assert.assertEquals(NUMBER_OF_ENTRIES, TASK_SERVICE.getSize());
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        Assert.assertNotNull(TASK_SERVICE.add(new Task()));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(TASK_LIST.size(), TASK_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final int actualTaskList = TASK_SERVICE.getSize(USER_ID_1);
        Assert.assertEquals(actualTaskList, TASK_SERVICE.getSize(USER_ID_1));
        Assert.assertEquals(0, TASK_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.getSize(null));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        TASK_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        TASK_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_ENTRIES, TASK_SERVICE.getSize());

        final int expectedNumberOfEntries = 0;
        TASK_SERVICE.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(USER_ID_1));
        Assert.assertNotEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.clear(null));
    }

    @Test
    public void testAddAll() {
        Assert.assertEquals(Collections.emptyList(), TASK_SERVICE.addAll(null));
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Task> taskListToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) taskListToAdd.add(new Task());
        Assert.assertNotNull(TASK_SERVICE.addAll(taskListToAdd));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testSet() {
        Assert.assertEquals(Collections.emptyList(), TASK_SERVICE.set(null));
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final List<Task> taskListToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES + 1; i++) taskListToAdd.add(new Task());
        Assert.assertNotNull(TASK_SERVICE.set(taskListToAdd));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        Assert.assertNotNull(TASK_SERVICE.remove(task));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> TASK_SERVICE.remove(null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        Assert.assertNotNull(TASK_SERVICE.removeById(task.getId()));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(null));
        Assert.assertThrows(EntityNotFoundException.class, () -> TASK_SERVICE.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(TASK_SERVICE.removeById(USER_ID_1, task.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(task.getId()));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById(null, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(UNKNOWN_ID, null));
        Assert.assertThrows(EntityNotFoundException.class, () -> TASK_SERVICE.removeById(UNKNOWN_ID, UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @Nullable final Task task = TASK_SERVICE.findOneByIndex(0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(TASK_SERVICE.removeByIndex(0));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertNull(TASK_REPOSITORY.findOneById(task.getId()));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.removeByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.removeByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(TASK_SERVICE.removeByIndex(USER_ID_1, 0));
        Assert.assertNull(TASK_SERVICE.findOneById(task.getId()));
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.removeByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.removeByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.removeByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 2;
        @NotNull final List<Task> taskListToRemove = new ArrayList<>();
        taskListToRemove.add(TASK_SERVICE.findAll().get(0));
        taskListToRemove.add(TASK_SERVICE.findAll().get(1));
        TASK_SERVICE.removeAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, TASK_SERVICE.getSize());
        TASK_SERVICE.removeAll(taskListToRemove);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        for (Task task : taskListToRemove) Assert.assertNull(TASK_SERVICE.findOneById(task.getId()));
    }

    @Test
    public void testFindOneById() {
        Assert.assertNull(TASK_SERVICE.findOneById(UNKNOWN_ID));
        Assert.assertNull(TASK_SERVICE.findOneById(null));
        for (@NotNull final Task task : TASK_LIST)
            Assert.assertEquals(task, TASK_SERVICE.findOneById(task.getId()));
    }

    @Test
    public void testFindOneByIdForUser() {
        Assert.assertNull(TASK_SERVICE.findOneById(null));
        Assert.assertNull(TASK_SERVICE.findOneById(UNKNOWN_ID));
        for (@NotNull final Task task : TASK_SERVICE.findAll(USER_ID_1))
            Assert.assertEquals(task, TASK_SERVICE.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findOneById(null, UNKNOWN_ID));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < TASK_SERVICE.getSize(); i++)
            Assert.assertEquals(TASK_LIST.get(i), TASK_SERVICE.findOneByIndex(i));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.findOneByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        for (int i = 0; i < TASK_SERVICE.getSize(USER_ID_1); i++)
            Assert.assertEquals(TASK_SERVICE.findAll(USER_ID_1).get(i), TASK_SERVICE.findOneByIndex(USER_ID_1, i));
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findOneByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.findOneByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.findOneByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.findOneByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAll();
        Assert.assertEquals(TASK_LIST, actualTaskList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAll(USER_ID_1);
        @NotNull final List<Task> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList, actualTaskList);
    }

    @Test
    public void testFindAllForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAll((String) null));
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAllByProjectId(USER_ID_1, PROJECT_ID);
        @NotNull final List<Task> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> PROJECT_ID.equals(p.getProjectId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList, actualTaskList);
        Assert.assertEquals(Collections.emptyList(), TASK_SERVICE.findAllByProjectId(USER_ID_1, null));
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(null, UNKNOWN_ID));
    }

    @Test
    public void testFindAllWithSort() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAll();

        actualTaskList.get(0).setStatus(Status.IN_PROGRESS);
        actualTaskList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, TASK_SERVICE.findAll(Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findAll(Sort.BY_STATUS).get(1).getStatus());

        actualTaskList.get(3).setName("A");
        actualTaskList.get(1).setName("B");
        Assert.assertEquals("A", TASK_SERVICE.findAll(Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", TASK_SERVICE.findAll(Sort.BY_NAME).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualTaskList.get(2).setCreated(date1);
        actualTaskList.get(3).setCreated(date2);
        Assert.assertEquals(date1, TASK_SERVICE.findAll(Sort.BY_CREATED).get(0).getCreated());
        Assert.assertEquals(date2, TASK_SERVICE.findAll(Sort.BY_CREATED).get(1).getCreated());

        @NotNull final List<Task> expectedTaskList = TASK_SERVICE.findAll();
        Assert.assertEquals(expectedTaskList, TASK_SERVICE.findAll((Sort) null));

        Assert.assertEquals(expectedTaskList, TASK_SERVICE.findAll());
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAll(USER_ID_1);

        actualTaskList.get(0).setStatus(Status.IN_PROGRESS);
        actualTaskList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, TASK_SERVICE.findAll(USER_ID_1, Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findAll(USER_ID_1, Sort.BY_STATUS).get(1).getStatus());

        actualTaskList.get(3).setName("A");
        actualTaskList.get(1).setName("B");
        Assert.assertEquals("A", TASK_SERVICE.findAll(USER_ID_1, Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", TASK_SERVICE.findAll(USER_ID_1, Sort.BY_NAME).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualTaskList.get(2).setCreated(date1);
        actualTaskList.get(3).setCreated(date2);
        Assert.assertEquals(date1, TASK_SERVICE.findAll(USER_ID_1, Sort.BY_CREATED).get(0).getCreated());
        Assert.assertEquals(date2, TASK_SERVICE.findAll(USER_ID_1, Sort.BY_CREATED).get(1).getCreated());

        @NotNull final List<Task> expectedTaskList = TASK_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(expectedTaskList, TASK_SERVICE.findAll(USER_ID_1, (Sort) null));
    }

    @Test
    public void testFindAllWithSortForUserForUser() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAll(null, Sort.BY_CREATED));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparator() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAll();
        @Nullable final Comparator<Task> comparatorByCreated = (Comparator<Task>) Sort.BY_CREATED.getComparator();
        @Nullable final Comparator<Task> comparatorByName = (Comparator<Task>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Task> comparatorByStatus = (Comparator<Task>) Sort.BY_STATUS.getComparator();

        actualTaskList.get(0).setStatus(Status.IN_PROGRESS);
        actualTaskList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, TASK_SERVICE.findAll(comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findAll(comparatorByStatus).get(1).getStatus());

        actualTaskList.get(3).setName("A");
        actualTaskList.get(1).setName("B");
        Assert.assertEquals("A", TASK_SERVICE.findAll(comparatorByName).get(0).getName());
        Assert.assertEquals("B", TASK_SERVICE.findAll(comparatorByName).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualTaskList.get(2).setCreated(date1);
        actualTaskList.get(3).setCreated(date2);
        Assert.assertEquals(date1, TASK_SERVICE.findAll(comparatorByCreated).get(0).getCreated());
        Assert.assertEquals(date2, TASK_SERVICE.findAll(comparatorByCreated).get(1).getCreated());

        @NotNull final List<Task> expectedTaskList = TASK_SERVICE.findAll();
        Assert.assertEquals(expectedTaskList, TASK_SERVICE.findAll((Comparator<Task>) null));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAll(USER_ID_1);
        @Nullable final Comparator<Task> comparatorByCreated = (Comparator<Task>) Sort.BY_CREATED.getComparator();
        @Nullable final Comparator<Task> comparatorByName = (Comparator<Task>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Task> comparatorByStatus = (Comparator<Task>) Sort.BY_STATUS.getComparator();

        actualTaskList.get(0).setStatus(Status.IN_PROGRESS);
        actualTaskList.get(4).setStatus(Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, TASK_SERVICE.findAll(USER_ID_1, comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findAll(USER_ID_1, comparatorByStatus).get(1).getStatus());

        actualTaskList.get(3).setName("A");
        actualTaskList.get(1).setName("B");
        Assert.assertEquals("A", TASK_SERVICE.findAll(USER_ID_1, comparatorByName).get(0).getName());
        Assert.assertEquals("B", TASK_SERVICE.findAll(USER_ID_1, comparatorByName).get(1).getName());

        @NotNull final Date date1 = new Date(1673824447000L);
        @NotNull final Date date2 = new Date(1674256447000L);
        actualTaskList.get(2).setCreated(date1);
        actualTaskList.get(3).setCreated(date2);
        Assert.assertEquals(date1, TASK_SERVICE.findAll(USER_ID_1, comparatorByCreated).get(0).getCreated());
        Assert.assertEquals(date2, TASK_SERVICE.findAll(USER_ID_1, comparatorByCreated).get(1).getCreated());

        @NotNull final List<Task> expectedTaskList = TASK_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(expectedTaskList, TASK_SERVICE.findAll(USER_ID_1, (Comparator<Task>) null));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUserNegative() {
        @Nullable final Comparator<Task> comparatorByCreated = (Comparator<Task>) Sort.BY_CREATED.getComparator();
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAll(null, comparatorByCreated));
    }

    @Test
    public void testCrateTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull final Task task1 = TASK_SERVICE.create(USER_ID_1, "NAME1", "DESCRIPTION");
        @NotNull final Task task2 = TASK_SERVICE.create(USER_ID_2, "NAME2");

        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertEquals("NAME1", task1.getName());
        Assert.assertEquals("DESCRIPTION", task1.getDescription());
        Assert.assertEquals(USER_ID_1, task1.getUserId());

        Assert.assertEquals("NAME2", task2.getName());
        Assert.assertEquals("", task2.getDescription());
        Assert.assertEquals(USER_ID_2, task2.getUserId());
    }

    @Test
    public void testCrateTaskNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, UNKNOWN_ID, null));
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final Task task = TASK_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusById(USER_ID_1, task.getId(), Status.COMPLETED));
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusById(USER_ID_1, task.getId(), null));
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, UNKNOWN_ID, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(UNKNOWN_ID, null, Status.COMPLETED));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(UNKNOWN_ID, UNKNOWN_ID, Status.COMPLETED));
    }

    @Test
    public void testChangeTaskStatusByIndex() {
        @NotNull final Task task = TASK_SERVICE.findAll(USER_ID_1).get(0);
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusByIndex(USER_ID_1, 0, Status.COMPLETED));
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusByIndex(USER_ID_1, 0, null));
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(null, 0, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(UNKNOWN_ID, -1, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(UNKNOWN_ID, null, Status.COMPLETED));
    }

    @Test
    public void testUpdateById() {
        @NotNull final Task task = TASK_SERVICE.findAll(USER_ID_1).get(0);
        @NotNull final String test = "TEST";
        Assert.assertNotNull(TASK_SERVICE.updateById(USER_ID_1, task.getId(), test, null));
        Assert.assertNotNull(TASK_SERVICE.updateById(USER_ID_1, task.getId(), test, test));
        Assert.assertEquals(test, task.getName());
        Assert.assertEquals(test, task.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, null, UNKNOWN_ID));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final Task task = TASK_SERVICE.findAll(USER_ID_1).get(0);
        @NotNull final String test = "TEST";
        Assert.assertNotNull(TASK_SERVICE.updateByIndex(USER_ID_1, 0, test, null));
        Assert.assertNotNull(TASK_SERVICE.updateByIndex(USER_ID_1, 0, test, test));
        Assert.assertEquals(test, task.getName());
        Assert.assertEquals(test, task.getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateByIndex(null, 0, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(UNKNOWN_ID, -1, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(UNKNOWN_ID, null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(UNKNOWN_ID, NUMBER_OF_ENTRIES, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateByIndex(USER_ID_1, 0, null, UNKNOWN_ID));
    }

}